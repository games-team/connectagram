# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Graeme Gott
# This file is distributed under the same license as the Connectagram package.
# 
# Translators:
# 79353a696ad19dc202b261b3067b7640_bec941e, 2015
msgid ""
msgstr ""
"Project-Id-Version: Connectagram\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-27 16:06-0400\n"
"PO-Revision-Date: 2014-11-13 13:41+0000\n"
"Last-Translator: 79353a696ad19dc202b261b3067b7640_bec941e, 2015\n"
"Language-Team: Greek (http://app.transifex.com/gottcode/connectagram/language/el/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: el\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../connectagram.appdata.xml.in:6 ../connectagram.desktop.in:4
msgid "Connectagram"
msgstr "Connectagram"

#: ../connectagram.appdata.xml.in:7 ../connectagram.desktop.in:6
msgid "Unscramble words placed in patterns"
msgstr "Αποκρυπτογραφήσετε λέξεις poz τοποθετήθηκαν σε μοτίβα"

#: ../connectagram.appdata.xml.in:10
msgid ""
"Connectagram is a word unscrambling game. The board consists of several "
"scrambled words that are joined together. You can choose the length of the "
"words, the amount of words, and the pattern that the words are arranged in. "
"The game provides a hint option for times when you are stuck, and features "
"an online word lookup that fetches the definitions of each word from "
"Wiktionary. Your current progress is automatically saved."
msgstr "Το Connectagram είναι ενα παιχνίδι αποκωδικοποίησης λέξεων.  Ο πίνακας αποτελείται από πολλά κωδικοποιημένα λόγια που ενώνονται μεταξύ τους. Μπορείτε να επιλέξετε το μήκος των λέξεων, το ποσό των λέξεων, και το μοτίβο που οι λέξεις διατάσσονται. Το παιχνίδι προσφέρει μια επιλογή υποδειξεων για τους χρόνους όταν δεν μπορείτε να ξεκολλήσετε, και διαθέτει μια ηλεκτρονική αναζήτηση λέξεων που φέρνει τους ορισμούς της κάθε λέξης από το Βικιλεξικό. Η πρόοδός σας αποθηκεύεται αυτόματα."

#: ../connectagram.desktop.in:5
msgid "Anagram Game"
msgstr "Aναγραμματισμός παιχνίδι"
