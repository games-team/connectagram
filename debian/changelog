connectagram (1.3.8-1) unstable; urgency=medium

  * New upstream release
  * Update copyright years

 -- Jonathan Carter <jcc@debian.org>  Thu, 20 Feb 2025 12:28:27 +0200

connectagram (1.3.6-1) unstable; urgency=medium

  * New upstream release
  * Clean up compiled word lists on dh_clean
    (Closes: #1044883)
  * Update standards version to 4.7.0

 -- Jonathan Carter <jcc@debian.org>  Thu, 09 Jan 2025 13:10:19 +0200

connectagram (1.3.5-2) unstable; urgency=medium

  * Team Upload
  * do not add runtime dependencies as build-dependencies (Closes: #1067459)

 -- Alexandre Detiste <tchet@debian.org>  Thu, 21 Mar 2024 23:07:00 +0100

connectagram (1.3.5-1) unstable; urgency=medium

  * New upstream release
  * Accept commit for updating alioth mailing list address

 -- Jonathan Carter <jcc@debian.org>  Wed, 24 Jan 2024 14:22:26 +0200

connectagram (1.3.4-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Mon, 19 Jun 2023 16:20:00 +0200

connectagram (1.3.3-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.6.2
  * Update copyright years

 -- Jonathan Carter <jcc@debian.org>  Wed, 08 Feb 2023 09:35:22 +0200

connectagram (1.3.2-1) unstable; urgency=medium

  * New upstream release
  * Remove usr/share/pixmaps from debian/install (no longer used)

 -- Jonathan Carter <jcc@debian.org>  Wed, 11 May 2022 10:48:51 +0200

connectagram (1.3.1-1) unstable; urgency=medium

  * New upstream release
  * Update watch file
  * New build dependencies:
    - qt6-tools-dev
    - qt6-l10n-tools
    - qt6-tools-dev-tools
    - mesa-common-dev
    - libgl1-mesa-dev
    - libglu1-mesa-dev
  * Update copyright years

 -- Jonathan Carter <jcc@debian.org>  Thu, 13 Jan 2022 12:09:31 +0200

connectagram (1.2.11-2) unstable; urgency=medium

  [ Pino Toscano ]
  * Drop the Debian-provided man page, as upstream already ships one.
  * Copy the upstream man page using connectagram.install rather than
    connectagram.manpages, as it is already installed in the proper location.
  * Drop the dirs file, as the upstream build system already creates all the
    needed directories.

  [ Jonathan Carter ]
  * Update standards version to 4.6.0
  * Remove unknown upstream metadata fields
  * Remove no longer needed as-needed linker flag

 -- Jonathan Carter <jcc@debian.org>  Mon, 06 Sep 2021 09:54:44 +0200

connectagram (1.2.11-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Jonathan Carter ]
  * New upstream release
  * Update standards version to 4.5.0
  * Upgrade debhelper-compat to 13
  * Declare Rules-Requires-Root: no

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 12 Apr 2020 15:12:29 +0000

connectagram (1.2.10-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Sat, 10 Aug 2019 18:33:38 +0200

connectagram (1.2.9-6) unstable; urgency=medium

  * Upgrade to standards version 4.4.0
  * Fix syntax in debian/copyright
  * Initial source-only upload

 -- Jonathan Carter <jcc@debian.org>  Tue, 09 Jul 2019 09:19:41 +0000

connectagram (1.2.9-5) unstable; urgency=medium

  * Fix copyright file problem

 -- Jonathan Carter <jcc@debian.org>  Thu, 04 Apr 2019 14:37:05 +0200

connectagram (1.2.9-4) unstable; urgency=medium

  * Fix debian games team e-mail address
  * Update debian/copyright

 -- Jonathan Carter <jcc@debian.org>  Sun, 24 Mar 2019 09:03:35 +0000

connectagram (1.2.9-2) unstable; urgency=medium

  * Add self to maintainers and update maintainer fields (Closes: #922910)
  * Upgrade to debian-compat (= 12)
  * Update standards version to 4.3.0

 -- Jonathan Carter <jcc@debian.org>  Fri, 22 Feb 2019 13:51:48 +0000

connectagram (1.2.9-1) unstable; urgency=medium

  * New upstream version.
  * Bumped to Standards-Version 4.1.5
    (no changes required).
  * Removed override_dh_installchangelogs from d/rules
    (the author has added a changelog file to the sources).

 -- Innocent De Marchi <tangram.peces@gmail.com>  Tue, 24 Jul 2018 10:50:12 +0100

connectagram (1.2.8-1) unstable; urgency=medium

  * New upstream version.
  * Removed add-icon-to-desktop.patch (has been added by the
    author in the sources).
  * Updated years on d/copyright (add fix typo error on e-mail).

 -- Innocent De Marchi <tangram.peces@gmail.com>  Sat, 23 Jun 2018 11:09:24 +0100

connectagram (1.2.7-1) unstable; urgency=medium

  * New upstream version.
  * Removed add-desktop-keyword.patch (has been added by the
    author in the sources).
  * Added icon field to .desktop file (with a patch).

 -- Innocent De Marchi <tangram.peces@gmail.com>  Sun, 10 Jun 2018 17:10:12 +0100

connectagram (1.2.6-1) unstable; urgency=medium

  * New upstream version 1.2.6 (Closes: #896443).
  * Updated dates and write secure URI in debian/copyright file.
  * Bumped to debhelper and compat level 11.
  * Bumped to Standards-Version 4.1.4.
  * Migrated to the salsa repository.
  * Install upstream manpage (removed Debian manpage).
  * Removed unnecessary dh argument --parallel on d/rules.
  * Added hardening flags on debian/rules.
  * Drop deprecated debian/menu file.
  * Added Multi-Arch field on debian/control.
  * Added debian/upstream/metadata file.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Sat, 21 Apr 2018 17:13:23 +0100

connectagram (1.2.4-1) unstable; urgency=medium

  * New upstream version 1.2.4
  * Updated dates in copyright
  * Added qttools5-dev-tools to Build-Deps

 -- Dariusz Dwornikowski <darek@debian.org>  Sat, 21 Jan 2017 13:56:57 +0100

connectagram (1.2.3-1) unstable; urgency=medium

  * New upstream version 1.2.3
  * Changed the maintainer email address to d.o
  * Updated dates in d/copyright
  * Refreshed the desktop file patch
  * Bump standards to 3.9.8
    - dropped debian menu system file
  * Updated VCS fields with https
  * Added hardening +all
  * Added the upstream changelog

 -- Dariusz Dwornikowski <darek@debian.org>  Tue, 11 Oct 2016 10:24:00 +0200

connectagram (1.2.1-2) unstable; urgency=medium

  * export QT_SELECT=5 in d/rules added (Closes: #777610)

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Fri, 13 Feb 2015 18:26:33 +0100

connectagram (1.2.1-1) unstable; urgency=medium

  * Imported Upstream version 1.2.1
  * add-desktop.keyword.patch refreshed
  * d/gdb.conf added
  * d/control
    - Bump standards to 3.9.6, no changes needed
    - removed libqt4-dev from Build-Deps
    - added qtbase5-dev to Build-Deps

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Tue, 10 Feb 2015 12:34:15 +0100

connectagram (1.1.2-2) unstable; urgency=medium

  * GPL-3+ in copyright (Closes: #747865)

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Tue, 13 May 2014 10:01:50 +0200

connectagram (1.1.2-1) unstable; urgency=medium

  * Imported Upstream version 1.1.2 (Closes: #726101)
  * Bump standards to 3.9.5 and compat to 9
  * New maintainer (Closes: #746459)
  * Split packages to connectagram and connectagram-data, containing word list
    files
  * Removed unneeded patch (Closes: #643082)
  * Migrated to dh
  * debian/copyright formatted according to DEP-5
  * Fixed watch file
  * Added menu file and keyword to desktop file (Closes: #737849)

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Fri, 02 May 2014 20:02:56 +0200

connectagram (1.0.1-1) unstable; urgency=low

  * Initial release (Closes: #580752)

 -- tang ke <tangk@lemote.com>  Fri, 07 May 2010 17:09:50 +0800
