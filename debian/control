Source: connectagram
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Jonathan Carter <jcc@debian.org>
Build-Depends:
  cmake,
  qt6-tools-dev,
  qt6-l10n-tools,
  qt6-tools-dev-tools,
  qt6-base-dev-tools,
  mesa-common-dev,
  debhelper-compat (= 13)
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://gottcode.org/connectagram/
Vcs-Git: https://salsa.debian.org/games-team/connectagram.git
Vcs-Browser: https://salsa.debian.org/games-team/connectagram

Package: connectagram
Architecture: any
Depends:
 connectagram-data (>= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Description: word unscrambling game
 The board consists of several scrambled words that are joined together.
 You can choose the length of the words, the amount of words, and the
 pattern that the words are arranged in. The game provides a hint option
 for times when you are stuck.

Package: connectagram-data
Architecture: all
Multi-Arch: foreign
Replaces: connectagram (<< 1.1.2-1)
Breaks: connectagram (<< 1.1.2-1)
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: word unscrambling game - data files
 The board consists of several scrambled words that are joined together.
 You can choose the length of the words, the amount of words, and the
 pattern that the words are arranged in. The game provides a hint option
 for times when you are stuck.
 .
 This package provides data files required by connectagram.
 They include word lists and translations.
